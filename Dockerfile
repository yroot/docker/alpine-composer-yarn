ARG PHP_IMAGE_VERSION="7.2"
FROM php:${PHP_IMAGE_VERSION}-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp
ENV COMPOSER_VERSION 1.6.3

RUN set -x \
    && apk update \
    && apk add --no-cache curl git subversion openssh openssl mercurial bash tar zip \
    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS libtool zlib-dev icu-dev coreutils\
	&& docker-php-ext-install \
		zip \
		pdo \
		pdo_mysql \
		mbstring \
		opcache \
		intl \
		json \
		tokenizer \
    && echo "memory_limit=-1" > "$PHP_INI_DIR/conf.d/memory-limit.ini" \
    && echo "date.timezone=${PHP_TIMEZONE:-UTC}" > "$PHP_INI_DIR/conf.d/date_timezone.ini"

RUN set -x \
    && cd /tmp \
    && wget -qO /tmp/composer-setup.php https://getcomposer.org/installer \
    && wget -qO /tmp/installer.sha384sum https://composer.github.io/installer.sha384sum \
    && sha384sum -c installer.sha384sum \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
    && composer --ansi --version --no-interaction \
    && rm -rf /tmp/* /tmp/.htaccess \
    && apk del .phpize-deps \
    && rm -rf /var/cache/apk/*

RUN set -x \
    && apk update \
    && apk add --no-cache --virtual .yarn-deps curl gnupg \
    && curl -o- -L https://yarnpkg.com/install.sh | sh \
    && apk del .yarn-deps

WORKDIR /app